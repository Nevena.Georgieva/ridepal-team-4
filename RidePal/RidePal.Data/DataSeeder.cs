﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RidePal.Data.Models;
using System;
using System.Collections.Generic;

namespace RidePal.Data
{
    public static class DataSeeder
    {
        public static void Seed(this ModelBuilder builder)
        {
            //Seed Roles
            //Normalized name won't let registering names only different by casing
            var roles = new List<Role>()
            {
               new Role{ Id=1,Name="Admin",NormalizedName="ADMIN"},
               new Role{Id=2,Name="User",NormalizedName="USER"},
            };
            builder.Entity<Role>().HasData(roles);

            //Seed Admin Account
            var hasher = new PasswordHasher<User>(); // Returns a hash version of the typed password
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(adminUser);               //Adding the user
            builder.Entity<IdentityUserRole<int>>().HasData(         //Many to many relationship with role
                new IdentityUserRole<int>()
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                }
            );

            //Seeding user accounts
            //Alice
            var regularUser = new User();
            regularUser.Id = 2;
            regularUser.UserName = "alice@alice.com";
            regularUser.NormalizedUserName = "ALICE@ALICE.COM";
            regularUser.Email = "alice@alice.com";
            regularUser.NormalizedEmail = "ALICE@ALICE.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "alice123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            // ^^A random value that must change whenever a users credentials change (password changed, login removed)
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );
        }
    }
}
