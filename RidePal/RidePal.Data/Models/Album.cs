﻿using Newtonsoft.Json;
using RidePal.Data.Models.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Album : Entity
    {
        [Required]
        public string TracklistURL { get; set; }

        [Required]
        public Genre Genre { get; set; }
        public int GenreId { get; set; }

        public  ICollection<Track> Tracks { get; set; } = new HashSet<Track>();
    }
}
