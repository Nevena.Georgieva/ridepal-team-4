﻿using System.Collections.Generic;

namespace RidePal.Data.Models.BingAPIModels
{
    public class BingAPIResources
    {
        public ICollection<BingAPIModel> resources = new List<BingAPIModel>();
    }
}
