﻿using System.Collections.Generic;

namespace RidePal.Data.Models.BingAPIModels
{
    public class BingAPIResourceSets
    {
        public ICollection<BingAPIResources> resourceSets = new List<BingAPIResources>();
    }
}
