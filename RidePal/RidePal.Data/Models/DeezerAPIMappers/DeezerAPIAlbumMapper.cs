﻿using RidePal.Data.Models.DeezerAPIModels;

namespace RidePal.Data.Models.DeezerAPIMappers
{
    public static class DeezerAPIAlbumMapper
    {
        public static Album GetModel(this DeezerAPIAlbum model)
        {
            var album = new Album
            {
                DeezerAPIId = model.id,
                Name = model.title,
                TracklistURL = model.tracklist
            };
            return album;
        }
    }

}
