﻿using Newtonsoft.Json;
using RidePal.Data.Models.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Artist : Entity
    {
        [Required]
        public string TrackListURL { get; set; }
    }
}
