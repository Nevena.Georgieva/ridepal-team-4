﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Playlist
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public int TotalPlaytime { get; set; }

        public int Rank { get; set; }

        public bool IsDeleted { get; set; }
       
        public User User { get; set; }

        public int UserId { get; set; }
       
        public ICollection<PlaylistTrack> PlaylistTracks { get; set; } = new List<PlaylistTrack>();

        public ICollection<PlaylistGenre> PlaylistGenres { get; set; } = new List<PlaylistGenre>();
    }
}
