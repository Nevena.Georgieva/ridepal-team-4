﻿namespace RidePal.Data.Models
{
    public class PlaylistGenre
    {
        public Playlist Playlist { get; set; }
        public int PlaylistId { get; set; }

        public Genre Genre { get; set; }
        public int GenreId { get; set; }
    }
}
