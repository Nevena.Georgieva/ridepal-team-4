﻿namespace RidePal.Data.Models
{
    public class PlaylistTrack
    {
        public Playlist Playlist { get; set; }
        public int PlaylistId { get; set; }

        public Track Track { get; set; }
        public int TrackId { get; set; }
    }
}
