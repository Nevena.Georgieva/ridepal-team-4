﻿using System.Collections.Generic;

namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIChartData
    {
        public ICollection<DeezerAPIPlaylist> data = new List<DeezerAPIPlaylist>();
    }
}
