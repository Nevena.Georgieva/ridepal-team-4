﻿namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIArtist
    {
        public int id { get; set; }

        public string name { get; set; }

        public string tracklist { get; set; }
    }
}