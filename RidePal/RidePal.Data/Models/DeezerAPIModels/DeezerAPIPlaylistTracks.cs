﻿namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIPlaylistTracks
    {
        public int TrackId { get; set; }

        public int PlaylistId { get; set; }
    }
}
