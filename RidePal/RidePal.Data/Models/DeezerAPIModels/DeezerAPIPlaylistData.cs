﻿using System.Collections.Generic;

namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIPlaylistData
    {
        public ICollection<DeezerAPITrack> data = new List<DeezerAPITrack>();
    }
}
