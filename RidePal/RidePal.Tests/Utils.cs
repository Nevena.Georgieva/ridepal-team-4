using Microsoft.EntityFrameworkCore;
using RidePal.Data.Context;
using RidePal.Data.Models;
using System.Collections.Generic;

namespace RidePal.Tests
{
    public class Utils
    {
        public static DbContextOptions<RidePalContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<RidePalContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static IEnumerable<Track> GetTracks()
        {
            return new Track[]
            {
                new Track
                {
                    Id = 1,
                    Title = "Track 1",
                    Duration = 300,
                    Rank = 100,
                    ArtistId = 1,
                    AlbumId = 1,
                    PreviewURL="https://www.google.com"
                },
                new Track
                {
                    Id = 2,
                    Title = "Track 2",
                    Duration = 300,
                    Rank = 200,
                    ArtistId = 2,
                    AlbumId = 2,
                    PreviewURL="https://www.yahoo.com"
                },
                new Track
                {
                    Id = 3,
                    Title = "Track 3",
                    Duration = 300,
                    Rank = 300,
                    ArtistId = 3,
                    AlbumId = 3
                },
                new Track
                {
                    Id = 4,
                    Title = "Track 4",
                    Duration = 300,
                    Rank = 400,
                    ArtistId = 3,
                    AlbumId = 3
                },
                new Track
                {
                    Id = 5,
                    Title = "Track 5",
                    Duration = 300,
                    Rank = 500,
                    ArtistId = 5,
                    AlbumId = 5
                },
                 new Track
                {
                    Id = 6,
                    Title = "Track 6",
                    Duration = 300,
                    Rank = 500,
                    ArtistId = 6,
                    AlbumId = 6
                },
                  new Track
                {
                    Id = 7,
                    Title = "Track 7",
                    Duration = 300,
                    Rank = 500,
                    ArtistId = 7,
                    AlbumId = 6
                },
                   new Track
                {
                    Id = 8,
                    Title = "Track 8",
                    Duration = 300,
                    Rank = 500,
                    ArtistId = 8,
                    AlbumId = 6
                },
                    new Track
                {
                    Id = 9,
                    Title = "Track 9",
                    Duration = 300,
                    Rank = 500,
                    ArtistId = 9,
                    AlbumId = 6
                },
            };
        }

        public static IEnumerable<Artist> GetArtists()
        {
            return new Artist[]
            {
                new Artist
                {
                    Id = 1,
                    Name = "Artist 1"
                },
                new Artist
                {
                    Id = 2,
                    Name = "Artist 2"
                },
                new Artist
                {
                    Id = 3,
                    Name = "Artist 3"
                },
                new Artist
                {
                    Id = 4,
                    Name = "Artist 4"
                },
                new Artist
                {
                    Id = 5,
                    Name = "Artist 5"
                },
                new Artist
                {
                    Id = 6,
                    Name = "Artist 6"
                },
                new Artist
                {
                    Id = 7,
                    Name = "Artist 7"
                },
                new Artist
                {
                    Id = 8,
                    Name = "Artist 8"
                },
                 new Artist
                {
                    Id = 9,
                    Name = "Artist 9"
                }
            };
        }

        public static IEnumerable<Album> GetAlbums()
        {
            return new Album[]
            {
                new Album
                {
                    Id = 1,
                    Name = "Album 1",
                    GenreId = 1
                },
                new Album
                {
                    Id = 2,
                    Name = "Album 2",
                    GenreId = 2
                },
                new Album
                {
                    Id = 3,
                    Name = "Album 3",
                    GenreId = 1
                },
                new Album
                {
                    Id = 4,
                    Name = "Album 4",
                    GenreId = 2
                },
                new Album
                {
                    Id = 5,
                    Name = "Album 5",
                    GenreId = 1
                },
                 new Album
                {
                    Id = 6,
                    Name = "Album 6",
                    GenreId = 3
                },
                 new Album
                {
                    Id = 7,
                    Name = "Album 7",
                    GenreId = 3
                },
                 new Album
                {
                    Id = 8,
                    Name = "Album 8",
                    GenreId = 3
                }
            };
        }

        public static IEnumerable<Genre> GetGenres()
        {
            return new Genre[]
            {
                new Genre
                {
                    Id = 1,
                    Name = "Genre 1"
                },
                new Genre
                {
                    Id = 2,
                    Name = "Genre 2"
                },
                 new Genre
                {
                    Id = 3,
                    Name = "Genre 3"
                },
                  new Genre
                {
                    Id = 4,
                    Name = "Genre 4"
                }
            };
        }

        public static IEnumerable<User> GetUsers()
        {
            return new User[]
            {
                    new User
                    {
                        Id = 1,
                        UserName = "User 1",
                        IsDeleted = false
                    },
                    new User
                    {
                        Id = 2,
                        UserName = "User 2",
                        IsDeleted = false
                    },
                    new User
                    {
                        Id = 3,
                        UserName = "User 3",
                        IsDeleted = true
                    }
            };
        }
        public static IEnumerable<Playlist> GetPlaylists()
        {
            return new Playlist[]
            {
                    new Playlist
                    {
                       Id = 1,
                       Title = "Playlist 1",
                       Rank = 100,
                       TotalPlaytime = 200,
                       UserId = 1,
                       IsDeleted = false
                    },
                    new Playlist
                    {
                       Id = 2,
                       Title = "Playlist 2",
                       Rank = 200,
                       TotalPlaytime = 100,
                       UserId = 2,
                       IsDeleted = true
                    },
                    new Playlist
                    {
                       Id = 3,
                       Title = "Playlist 3",
                       Rank = 300,
                       TotalPlaytime = 900,
                       UserId = 3,
                       IsDeleted = false
                    },
                     new Playlist
                    {
                       Id = 4,
                       Title = "Playlist 4",
                       Rank = 300,
                       TotalPlaytime = 800,
                       UserId = 3,
                       IsDeleted = false
                    }
            };
        }

        public static IEnumerable<PlaylistTrack> GetPlaylistTracks()
        {
            return new PlaylistTrack[]
            {
                    new PlaylistTrack
                    {
                        PlaylistId = 1,
                        TrackId = 1
                    },
                    new PlaylistTrack
                    {
                        PlaylistId = 1,
                        TrackId = 2
                    },
                    new PlaylistTrack
                    {
                        PlaylistId = 1,
                        TrackId = 3
                    }
            };
        }
        public static IEnumerable<PlaylistGenre> GetPlaylistGenres()
        {
            return new PlaylistGenre[]
            {
                    new PlaylistGenre
                    {
                        PlaylistId = 1,
                        GenreId = 3
                    },
                    new PlaylistGenre
                    {
                        PlaylistId = 1,
                        GenreId = 1
                    },
                    new PlaylistGenre
                    {
                        PlaylistId = 2,
                        GenreId = 1
                    },
                    new PlaylistGenre
                    {
                        PlaylistId = 2,
                        GenreId = 3
                    },
                    new PlaylistGenre
                    {
                        PlaylistId = 3,
                        GenreId = 3
                    },
                    new PlaylistGenre
                    {
                        PlaylistId = 4,
                        GenreId = 4
                    }
            };
        }
    }
}
