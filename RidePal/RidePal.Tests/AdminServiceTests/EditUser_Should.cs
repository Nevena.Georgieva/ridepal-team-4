﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.AdminServiceTests
{
    [TestClass]
    public class EditUser_Should
    {
        [TestMethod]
        public async Task EditUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditUser_When_ParamsAreValid));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);
                var result = await sut.EditUserAsync(2, "New name");

                Assert.AreEqual("New name", actContext.Users.Skip(1).First().UserName);
            }
        }
        [TestMethod]
        public async Task ReturnFalse_When_UserDoesntExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_UserDoesntExist));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                var result = await sut.EditUserAsync(5, "New name");

                Assert.IsFalse(result);
            }
        }
        [TestMethod]
        public async Task ReturnTrue_When_UserExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_UserExists));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                var result = await sut.EditUserAsync(2, "New name");

                Assert.IsTrue(result);
                Assert.AreEqual("New name", actContext.Users.Skip(1).First().UserName);
            }
        }

        [TestMethod]
        public async Task EditUser_Should_Throw_When_InvalidParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditUser_Should_Throw_When_InvalidParameters));

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.EditUserAsync(0, "New name"));
            }
        }

        [TestMethod]
        public async Task EditUser_Should_Throw_When_InvalidString()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditUser_Should_Throw_When_InvalidString));

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditUserAsync(1, "    "));
            }
        }
    }
}
