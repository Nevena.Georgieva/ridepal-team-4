﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.AdminServiceTests
{
    [TestClass]
    public class DeleteUser_Should
    {
        [TestMethod]
        public async Task DeleteUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeleteUser_When_ParamsAreValid));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);
                var result = await sut.DeleteUserAsync(2);

                Assert.IsTrue(result);
                Assert.IsTrue(actContext.Users.Skip(1).First().IsDeleted);
            }
        }
        [TestMethod]
        public async Task Throw_When_UserDoesntExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserDoesntExist));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteUserAsync(5));
            }
        }
        [TestMethod]
        public async Task ReturnFalse_When_UserIsAlreadyDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_UserIsAlreadyDeleted));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                var result = await sut.DeleteUserAsync(3);

                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task DeleteUser_Should_Throw_When_InvalidParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeleteUser_Should_Throw_When_InvalidParameters));

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.DeleteUserAsync(0));
            }
        }
    }
}
