﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServiceTests
{
    [TestClass]
   public class ListenToPlaylist_Should
    {
        [TestMethod]
        public async Task ReturnCorrecTrackDTOs_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrecTrackDTOs_When_ParamsAreValid));

            var tracks = Utils.GetTracks();
            var playlistTracks = Utils.GetPlaylistTracks();
    

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Tracks.AddRange(tracks);
                arrangeContext.PlaylistTracks.AddRange(playlistTracks);
                arrangeContext.SaveChanges();
            }

           //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                var collection = await sut.ListenToPlaylistAsync(1);

                string url1 = "https://www.google.com";
                string url2 = "https://www.yahoo.com";

                Assert.AreEqual(collection.First().PreviewURL, url1);
                Assert.AreEqual(collection.Skip(1).First().PreviewURL,url2);
            }
        }

        [TestMethod]
        public async Task ListenToPlaylists_Should_Throw_When_InvalidParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ListenToPlaylists_Should_Throw_When_InvalidParameters));

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.ListenToPlaylistAsync(0));
            }
        }
    }
}
