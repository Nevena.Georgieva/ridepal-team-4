﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GetTracks_Should
    {
        [TestMethod]
        public async Task ReturnTracks_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTracks_When_ParamsAreValid));
            var tracks = Utils.GetTracks();
            var playlists = Utils.GetPlaylists();
            var playlistTracks = Utils.GetPlaylistTracks();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Tracks.AddRange(tracks);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.PlaylistTracks.AddRange(playlistTracks);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                var collection = await sut.GetTracksAsync(1);

                Assert.AreEqual(3, collection.Count());
            }
        }

        [TestMethod]
        public async Task Throw_When_ParamsAreNotValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_ParamsAreNotValid));
        
            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

               await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() =>  sut.GetTracksAsync(0));
            }
        }
        [TestMethod]
        public async Task Throw_When_PlaylistDoesntExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_PlaylistDoesntExist));

            var tracks = Utils.GetTracks();
            var playlists = Utils.GetPlaylists();
            var playlistTracks = Utils.GetPlaylistTracks();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Tracks.AddRange(tracks);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.PlaylistTracks.AddRange(playlistTracks);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetTracksAsync(10));
            }
        }
    }
}
