using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class GeneratePlaylist_Should
    {
        [TestMethod]
        public async Task CreateCorrectPlaylist_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CreateCorrectPlaylist_When_ParamsAreValid));

            var tracks = Utils.GetTracks();
            var artists = Utils.GetArtists();
            var albums = Utils.GetAlbums();
            var genres = Utils.GetGenres();
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Tracks.AddRange(tracks);
                arrangeContext.Artists.AddRange(artists);
                arrangeContext.Albums.AddRange(albums);
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();
                MapServiceMock.Setup(map => map.GetTravelTimeAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(900);

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var user = actContext.Users.First();
                var result = await sut.GeneratePlaylistAsync(new List<Tuple<string, int>> {
                    new Tuple<string, int>(item1: "Genre 1", item2: 80),
                    new Tuple<string, int>(item1: "Genre 2", item2: 20) },
                    "sofia", "pernik", "MyPlayList", user.Id);

                //Assert
                Assert.AreEqual(1, result.Id);
                Assert.AreEqual("MyPlayList", result.Title);
                Assert.AreEqual(1200, result.TotalPlaytime);
                Assert.IsTrue(result.AverageRank > 0);
                Assert.AreEqual(1, actContext.Playlists.First().Id);
                Assert.AreEqual("MyPlayList", actContext.Playlists.First().Title);
                Assert.AreEqual(1200, actContext.Playlists.First().TotalPlaytime);
                Assert.AreEqual(2, actContext.Playlists.First().PlaylistGenres.Count());
                Assert.AreEqual("Genre 1", actContext.Playlists.First().PlaylistGenres.First().Genre.Name);
                Assert.AreEqual("Genre 2", actContext.Playlists.First().PlaylistGenres.Last().Genre.Name);
                Assert.AreEqual(4, actContext.Playlists.First().PlaylistTracks.Count());
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_InvalidParameters));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GeneratePlaylistAsync(null, null, null, null, 0));
            }
        }
    }
}
