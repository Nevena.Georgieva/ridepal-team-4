using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class GetPlaylistsForUserCount_Should
    {
        [TestMethod]
        public async Task GetPlaylistForUserCount_Should_ReturnCorrectCount_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylistForUserCount_Should_ReturnCorrectCount_When_ParamsAreValid));

            var playlists = Utils.GetPlaylists();
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.GetPlaylistsForUserCountAsync(3);

                //Assert
                Assert.AreEqual(2, result);
            }
        }

        [TestMethod]
        public async Task GetPlaylistForUserCount_Should_Throw_When_InvalidInput()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylistForUserCount_Should_Throw_When_InvalidInput));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetPlaylistsForUserCountAsync(0));
            }
        }
    }
}
