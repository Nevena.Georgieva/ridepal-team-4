using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class DeletePlaylist_Should
    {
        [TestMethod]
        public async Task DeletePlaylist_Should_ReturnTrueAndUpdateDb_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePlaylist_Should_ReturnTrueAndUpdateDb_When_ParamsAreValid));

            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.DeletePlaylistAsync(1);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(true, actContext.Playlists.First().IsDeleted);
            }
        }

        [TestMethod]
        public async Task DeletePlaylist_Should_ReturnFalse_When_PlaylistIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePlaylist_Should_ReturnFalse_When_PlaylistIsDeleted));

            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.DeletePlaylistAsync(2);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task DeletePlaylist_Should_Throw_When_InvalidId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePlaylist_Should_Throw_When_InvalidId));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.DeletePlaylistAsync(0));
            }
        }

        [TestMethod]
        public async Task DeletePlaylist_Should_Throw_When_NoSuchPlaylist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePlaylist_Should_Throw_When_NoSuchPlaylist));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeletePlaylistAsync(5));
            }
        }
    }
}
