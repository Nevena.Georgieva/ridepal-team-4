using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class GetPlaylistsForUser_Should
    {
        [TestMethod]
        public async Task GetPlaylistForUser_Should_ReturnCorrectPlaylistDTOCollection_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylistForUser_Should_ReturnCorrectPlaylistDTOCollection_When_ParamsAreValid));

            var genres = Utils.GetGenres();
            var playlists = Utils.GetPlaylists();
            var playlistGenres = Utils.GetPlaylistGenres();
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.PlaylistGenres.AddRange(playlistGenres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.GetPlaylistsForUserAsync(3);

                //Assert
                Assert.AreEqual(2, result.Count());
                Assert.AreEqual("Playlist 3", result.First().Title);
                Assert.AreEqual("Playlist 4", result.Last().Title);
                Assert.AreEqual(1, result.First().Genres.Count());
                Assert.AreEqual(1, result.Last().Genres.Count());
                Assert.IsTrue(result.First().Genres.Contains("Genre 3"));
                Assert.IsTrue(result.Last().Genres.Contains("Genre 4"));
                Assert.IsFalse(result.First().Genres.Contains("Genre 2"));
            }
        }

        [TestMethod]
        public async Task GetPlaylist_Should_Throw_When_InvalidId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylist_Should_Throw_When_InvalidId));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetPlaylistsForUserAsync(0));
            }
        }
    }
}
