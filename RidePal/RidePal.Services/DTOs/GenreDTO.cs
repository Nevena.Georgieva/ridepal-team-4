﻿namespace RidePal.Services.DTOs
{
    public  class GenreDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
    }
}