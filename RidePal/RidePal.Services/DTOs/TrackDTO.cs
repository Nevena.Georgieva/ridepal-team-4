﻿namespace RidePal.Services.DTOs
{
    public class TrackDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Link { get; set; }

        public int Duration { get; set; }

        public int Rank { get; set; }

        public string PreviewURL { get; set; }

        public string ArtistName { get; set; }
        public int ArtistId { get; set; }

        public string AlbumName { get; set; }
        public int AlbumId { get; set; }
    }
}

