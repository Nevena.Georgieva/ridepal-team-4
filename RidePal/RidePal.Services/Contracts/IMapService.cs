﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IMapService
    {
        Task<int> GetTravelTimeAsync(string wp0, string wp1);
    }
}
