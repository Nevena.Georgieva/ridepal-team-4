﻿using RidePal.Data.Models;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);
    }
}
