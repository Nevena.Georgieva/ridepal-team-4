﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IAdminService
    {
        Task<IEnumerable<UserDTO>> GetAllUsersAsync();

        Task<UserDTO> GetUserAsync(int id);

        Task<UserDTO> GetUserByNameAsync(string name);

        Task<bool> EditUserAsync(int userId, string Name);

        Task<bool> DeleteUserAsync(int userId);
    }
}
