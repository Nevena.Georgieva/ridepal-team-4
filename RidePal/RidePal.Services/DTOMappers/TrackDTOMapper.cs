﻿using RidePal.Data.Models;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace RidePal.Services.DTOMappers
{
    public static class TrackDTOMapper
    {
        public static TrackDTO GetModel(this Track model)
        {
            var dto = new TrackDTO
            {
                Id = model.Id,
                Title = model.Title,
                Duration = model.Duration,
                Link = model.Link,
                Rank = model.Rank,
                PreviewURL = model.PreviewURL
            };
            return dto;
        }
        public static IEnumerable<TrackDTO> GetModel(this ICollection<Track> items)
        {
            return items.Select(tr=>tr.GetModel()).ToList();
        }
    }
}
