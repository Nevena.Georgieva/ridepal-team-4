﻿using Microsoft.EntityFrameworkCore;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.DTOMappers;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class PlaylistService : IPlaylistService
    {
        private readonly RidePalContext dbContext;

        public PlaylistService(RidePalContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Returns all playlists filtered by certain 'criteria' and 'input'. If filters are null or whiteSpace, returns
        /// all playlists ordered by descending 'Rank' property.
        /// </summary>
        /// <param name="criteria">The criteria the user must choose from: name, genre, duration.</param>
        /// <param name="input">The second filter, the matching string, that the user must enter.</param>
        /// <returns>Collection of PlaylistDTOs</returns>
        public async Task<IEnumerable<PlaylistDTO>> FilterPlaylistsAsync(string criteria, string input)
        {

            var collection = await dbContext.Playlists
                .Where(p => p.IsDeleted == false)
                .Include(p => p.User)
                .Include(p => p.PlaylistGenres)
                .ThenInclude(p => p.Genre)
                .ToListAsync();

            if (string.IsNullOrWhiteSpace(input))
            {
                var orderedCollection = await dbContext.Playlists
                     .Where(p => p.IsDeleted == false)
                     .OrderByDescending(playlist => playlist.Rank)
                     .ToListAsync();

                var resultCollection = orderedCollection.GetModel();

                foreach (var playlist in resultCollection)
                {
                    var tempGenres = await GetPlaylistGenres(playlist.Id);
                    playlist.Genres = tempGenres;
                }

                return resultCollection;
            }

            switch (criteria)
            {
                case "name":
                    collection = collection.Where(p => p.Title.ToLower().Contains(input.ToLower())).ToList();
                    break;

                case "genre":
                    List<Playlist> result = new List<Playlist>();
                    foreach (var playlist in collection)
                    {
                        foreach (var playlistGenre in playlist.PlaylistGenres)
                        {
                            if (playlistGenre.Genre.Name.ToLower().Contains(input.ToLower()))
                            {
                                result.Add(playlist);
                            }
                        }
                    }
                    collection = result;
                    break;

                case "duration": //will return only those playlists with duration = requested(minutes) +/- 5 minutes
                    collection = collection.Where(p => (p.TotalPlaytime >= int.Parse(input) * 60 - 300) &&
                                                       (p.TotalPlaytime <= int.Parse(input) * 60 + 300)).ToList();
                    break;

                default:
                    break;
            }

            var playlistDTOs = collection.GetModel();
            foreach (var playlist in playlistDTOs)
            {
                var tempGenres = await GetPlaylistGenres(playlist.Id);
                playlist.Genres = tempGenres;
            }

            return playlistDTOs;
        }

        /// <summary>
        /// Filters the given collection by 'pageIndex' and/or 'itemsPerPage'.
        /// </summary>
        /// <param name="collection">The collection to filter.</param>
        /// <param name="itemsPerPage">The number of items to show per page.</param>
        /// <param name="pageIndex">The page number the user is currently viewing.</param>
        /// <returns>Collection of PlaylistDTOs</returns>
        public IEnumerable<PlaylistDTO> FilterPlaylistPaging(IEnumerable<PlaylistDTO> collection, int itemsPerPage = 5, int pageIndex = 1)
        {
            return collection
                 .Skip((pageIndex - 1) * itemsPerPage)
                 .Take(itemsPerPage);
        }

        /// <summary>
        /// Filters the given collection by 'pageIndex' and/or 'itemsPerPage'.
        /// </summary>
        /// <param name="itemsPerPage">The number of items to show per page.</param>
        /// <param name="pageIndex">The page number the user is currently viewing.</param>
        /// <returns>Collection of PlaylistDTOs</returns>
        public async Task<IEnumerable<PlaylistDTO>> GetAllPlaylistsAsync(int itemsPerPage = 5, int pageIndex = 1)
        {
            var collection = await dbContext.Playlists
                 .Where(p => p.IsDeleted == false)
                 .OrderByDescending(playlist => playlist.Rank)
                 .Skip((pageIndex - 1) * itemsPerPage)
                 .Take(itemsPerPage)
                 .ToListAsync();

            IEnumerable<PlaylistDTO> playlistDTOs = collection.GetModel();
            foreach (var playlist in playlistDTOs)
            {
                var tempGenres = await GetPlaylistGenres(playlist.Id);
                playlist.Genres = tempGenres;
            }

            return playlistDTOs;
        }

        /// <summary>
        /// Returns the count of all available playlists in the DB.
        /// </summary>
        public async Task<int> GetPlaylistCountAsync() => await dbContext.Playlists.CountAsync();

        /// <summary>
        /// Returns the count of the filtered playlists from DB.
        /// </summary>
        /// <param name="collection">The collection of playlistDTOs.</param>
        public int GetFilterPlaylistCountAsync(IEnumerable<PlaylistDTO> collection) => collection.Count();

        /// <summary>
        /// Returns the count of all tracks in a given playlist.
        /// </summary>
        /// <param name="playlistId">The id of the playlist.</param>
        public async Task<int> GetTracksCountAsync(int playlistId) => await dbContext.PlaylistTracks
                .Where(t => t.PlaylistId == playlistId)
                .CountAsync();

        /// <summary>
        /// Returns the tracks from a given playlist.
        /// </summary>
        /// <param name="id">The id of the playlist.</param>
        /// <param name="itemsPerPage">The number of items to show per page.</param>
        /// <param name="pageIndex">The page number the user is currently viewing.</param>
        /// <returns>Collection of TrackDTOs</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if the id is less than '1'.</exception>
        /// <exception cref="System.ArgumentException">Throws if the playlist is not found in the DB.</exception>
        public async Task<IEnumerable<TrackDTO>> GetTracksAsync(int id, int itemsPerPage = 5, int pageIndex = 1)
        {
            if (id < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (!dbContext.Playlists.Any(p => p.Id == id))
            {
                throw new ArgumentException();
            }

            var collection = await dbContext.PlaylistTracks
                .Include(p => p.Track)
                .Where(t => t.PlaylistId == id)
                .Skip((pageIndex - 1) * itemsPerPage)
                 .Take(itemsPerPage)
                .Select(x => x.Track.GetModel())
                .ToListAsync();

            foreach (var item in collection)
            {
                item.ArtistName = await GetArtistsAsync(item.Id);
            }

            return collection;
        }

        /// <summary>
        /// Returns the tracks of a given playlist for the purpose of listening to the audio previews.
        /// </summary>
        /// <param name="playlistId">The id of the playlist.</param>
        /// <returns>Collection of TrackDTOs</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if the id is less than '1'.</exception>
        /// <exception cref="System.ArgumentException">Throws if the playlist is not found in the DB.</exception>
        public async Task<IEnumerable<TrackDTO>> ListenToPlaylistAsync(int playlistId)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var trackPreviewURLs = await dbContext.PlaylistTracks
                .Include(pt => pt.Track)
                .Where(p => p.PlaylistId == playlistId)
                .Select(pt => pt.Track.GetModel()).ToListAsync();

            if (trackPreviewURLs.Count() == 0)
            {
                throw new ArgumentException();
            }

            return trackPreviewURLs;
        }

        /// <summary>
        /// Returns the name of the artist for a given track.
        /// </summary>
        /// <param name="id">The id of the track.</param>
        /// <returns>A string with the name of tha artist.</returns>
        private async Task<string> GetArtistsAsync(int id)
        {
            var artistsName = await dbContext.Tracks

                .Include(t => t.Artist)
                .Where(p => p.Id == id)
                .Select(t => t.Artist.Name)
                .FirstOrDefaultAsync();

            return artistsName;
        }

        /// <summary>
        /// Returns the genres for a given playlist.
        /// </summary>
        /// <param name="playlistId">The id of the playlist.</param>
        /// <returns>A string containing the titles of the genres.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if the id is less than '1'.</exception>
        private async Task<IEnumerable<string>> GetPlaylistGenres(int playlistId)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var genreNames = await dbContext.PlaylistGenres
               .Include(p => p.Genre)
               .Where(p => p.PlaylistId == playlistId)
               .Select(pg => pg.Genre.Name).ToListAsync();

            return genreNames;
        }
    }
}
