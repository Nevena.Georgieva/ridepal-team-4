﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Web.APIControllers
{
    /// <summary>
    /// The controller performs CRUD operation on users.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    [Authorize(Roles = "Admin")]
    [Route("api/adm")]
    [ApiController]
    public class AdmController : ControllerBase
    {
        private readonly IAdminService service;

        public AdmController(IAdminService service)
        {
            this.service = service;
        }

        // GET: api/adm
        /// <summary>
        /// Retrieves all users.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="401">Request is available only for authorized users</response>
        /// <response code="403">The user is not with role admin</response>
        /// <response code="404">No users found in DB</response>
        /// <response code="500">Oops! Can't list the users right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers()
        {
            IEnumerable<UserDTO> users = null;
            try
            {
                users = await service.GetAllUsersAsync();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return Ok(users);
        }

        //GET api/adm/id
        /// <summary>
        /// Retrieves a user specified by id.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized users</response>
        /// <response code="403">The user is not with role admin</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the user right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            UserDTO user = null;
            try
            {
                user = await this.service.GetUserAsync(id);
            }
            catch (Exception)
            {
                return NotFound("No user found");
            }

            return Ok(user);
        }

        //GET api/adm/username/somenamehere
        /// <summary>
        /// Retrieves a user specified by name.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized users</response>
        /// <response code="403">The user is not with role admin</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the user right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("username/{name}")]
        public async Task<IActionResult> GetUser(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest();
            }
            UserDTO user = null;
            try
            {
                user = await this.service.GetUserByNameAsync(name);
            }
            catch (ArgumentException)
            {
                return NotFound("No user found");
            }

            return Ok(user);
        }

        //PUT: api/adm/5?name=somename
        /// <summary>
        /// Edits user name.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized users</response>
        /// <response code="403">The user is not with role admin</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't edit the user right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, [FromQuery] string name)
        {
            if (id < 1 || string.IsNullOrWhiteSpace(name))
            {
                return BadRequest();
            }
            else
            {
                var temp = await service.EditUserAsync(id, name);

                if (temp == false)
                {
                    return NotFound();
                }
                return Ok();
            }
        }

        //DELETE: api/adm/5
        /// <summary>
        /// Deletes user.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized users</response>
        /// <response code="403">The user is not with role admin</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't delete the user right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            else
            {
                try
                {
                    var temp = await service.DeleteUserAsync(id);

                    if (temp == false)
                    {
                        return NotFound();
                    }
                }
                catch (ArgumentException)
                {
                    return NotFound();
                }

                return Ok();
            }
        }
    }
}
