﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IAdminService adminService;
        private readonly ISeedService seedService;
        private readonly ILogger<AccountController> logger;

        public AdminController(IAdminService adminService, ISeedService seedService, ILogger<AccountController> logger)
        {
            this.adminService = adminService ?? throw new ArgumentNullException(nameof(adminService));
            this.seedService = seedService ?? throw new ArgumentNullException(nameof(seedService));
            this.logger = logger;
        }

        // GET: Admin
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            IEnumerable<UserDTO> users = null;
            try
            {
                users = await this.adminService.GetAllUsersAsync();
            }
            catch (Exception)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            var AllUsers = users
            .Select(x => new UserViewModel(x));

            return View(AllUsers);
        }

        // GET: Admin/Edit/5
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            UserDTO user = null;
            try
            {
                user = await this.adminService.GetUserAsync(id);
            }
            catch (Exception)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            var result = new UserViewModel(user);
            return View(result);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await this.adminService.EditUserAsync(user.Id, user.Name);
                }
                catch (Exception)
                {
                    return View("Views/Home/NoContent.cshtml");
                }

                logger.LogInformation("Admin edited name of user with id: {0} to {1}.", user.Id, user.Name);
                return RedirectToAction(nameof(Index));
            }

            return View(user);
        }

        // GET: Admin/Delete/5
        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            UserDTO userDTO = null;
            try
            {
                userDTO = await this.adminService.GetUserAsync(id);
            }
            catch (Exception)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            UserViewModel result = new UserViewModel(userDTO);
            return View(result);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await this.adminService.DeleteUserAsync(id);

            if (result == true)
            {
                logger.LogInformation("Admin deleted user with id: {0}.", id);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("Views/Home/BadRequest.cshtml");
            }
        }

        // POST: Admin/Sync
        [HttpGet]
        public async Task<IActionResult> Sync()
        {
            try
            {
                await this.seedService.GetDataFromExternalAPIAsync();
            }
            catch (Exception)
            {
                logger.LogInformation("Syncing(seeding) DataBase failed.");
                return View("Views/Home/NoContent.cshtml");
            }

            logger.LogInformation("DataBase was synced(seeded).");
            return RedirectToAction(nameof(Index));
        }
    }
}
