﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services.Contracts;
using RidePal.Web.Models;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPlaylistService playlistService;
        private readonly ILogger<AccountController> logger;

        public HomeController(IPlaylistService playlistService, ILogger<AccountController> logger)
        {
            this.playlistService = playlistService ?? throw new ArgumentNullException(nameof(playlistService));
            this.logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var topRated = await playlistService.GetAllPlaylistsAsync();
            var viewModels = topRated.Take(3).Select(p => new PlaylistViewModel(p)).ToList();
            logger.LogInformation("Requested Index page.");

            return View(viewModels);
        }

        public IActionResult AboutUs()
        {
            logger.LogInformation("Requested AboutUs page.");
            return View();
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
