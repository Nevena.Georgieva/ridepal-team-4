﻿var playlistData;
var elementPerPage = 5;

$("#search-input").on('keydown', function (event) {
    if (event.key == "Enter") event.preventDefault();
});

function getPlaylistData(page) {

    let SearchBy = $('#SearchBy').val(); // criteria
    let SearchVal = $('#search-input').val(); //input
    let setData = $('#DataSearhing'); //table filled according to response
    setData.html(""); //set html

    const tableContainer = document.getElementById('DataSearhing'); //*******

    $.ajax(
        {
            type: "get",
            url: "/Playlist/FilterOnKeyUp?criteria=" + SearchBy + "&input=" + SearchVal + '&pageNum=' + page,
            contentType: "html",
            success: function (response) {
                if (response.count == 0) {
                    setData.append('<tr style="color:#dc3545"><td colspan="3"><b>No Matching Data</b></td></tr>')
                }
                else {
                    playlistData = response.collection;
                    document.getElementById('paging').innerHTML = null;

                    var pagingContainer = document.getElementById('paging'); //getting the pagination from the html

                    for (var i = 1; i <= response.count; i++) {
                        if (i % 5 == 0 || i == response.count) {
                            var pageNum = i / 5;
                            if (i % 5 > 0) {
                                pageNum++;
                            }
                            var pagingElement = document.createElement('button'); //Creating the page button
                            pagingElement.setAttribute('id', 'pbtn');
                            pagingElement.onclick = (event) => getPlaylistData(event.target.value * 1) //*
                            pagingElement.innerHTML = Math.floor(pageNum);
                            pagingElement.value = Math.floor(pageNum) // value of the button
                            pagingContainer.appendChild(pagingElement); //adding the button to the container
                        }
                    }

                    $.each(response.models, function (index, value) {
                        if (index == elementPerPage) {
                            return;
                        }
                        row = document.createElement('tr');

                        //Title
                        title = document.createElement('td');
                        title.innerHTML = value.title;

                        //AverageRank
                        rank = document.createElement('td');
                        rank.innerHTML = value.averageRank;

                        //Duration
                        duration = document.createElement('td');
                        duration.innerHTML = value.totalDuration;

                        //Genres **********
                        genres = document.createElement('td');
                        genres.innerHTML = value.genres;


                        //Details
                        details = document.createElement('td');
                        const anchor = `<center><a class="gg-readme" href="/Playlist/Details/${value.id}"></a></center>`;
                        details.innerHTML = (anchor);

                        //Play
                        play = document.createElement('td');
                        const anchor2 = `<center><a class="gg-play-button-o" href="/Playlist/Listen/${value.id}"></a></center>`;
                        play.innerHTML = (anchor2);

                        // title, rank , duration, genre, play
                        row.appendChild(title);
                        row.appendChild(rank);
                        row.appendChild(duration).append(" mins");
                        row.appendChild(genres);
                        row.appendChild(details);
                        row.appendChild(play);

                        // add other fields
                        if (window.userRole === "admin") {
                            edit = document.createElement('td');
                            const anchor3 = `<center><a class="fa fa-edit" href="/UserPlaylist/Edit/${value.id}"></a></center>`;
                            edit.innerHTML = (anchor3);

                            deleteData = document.createElement('td');
                            const anchor4 = `<center><a class="fa fa-close" href="/UserPlaylist/Delete/${value.id}"></a></center>`;
                            deleteData.innerHTML = (anchor4);
                            // add other buttons
                            row.appendChild(edit);
                            row.appendChild(deleteData);
                        }
                        tableContainer.appendChild(row);
                    });
                }
            }
        });
}

$('#search-input').keyup(() => getPlaylistData(1)); // Passing the page value



