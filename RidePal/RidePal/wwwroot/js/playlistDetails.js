﻿var playlistData;
var elementPerPage = 5;
var url = window.location.pathname;

var id = url.substring(url.lastIndexOf('/') + 1);

function pagebtnclick(e) {
    var page = e.target.value * 1;
    getPlaylistData(page);
}

function getPlaylistData(page) {

    let setData = $('#DataSearhing'); //table filled according to response
    setData.html(""); //set html
    const tableContainer = document.getElementById('DataSearhing'); //*******

    $.ajax(
        {
            type: "get",
            url: "/Playlist/DetailsJson?id=" + id +"&pageNum=" + page, // add id
            contentType: "json",
            success: function (response) {

                    playlistData = response.models;
                    document.getElementById('paging').innerHTML = null;

                    var pagingContainer = document.getElementById('paging'); //getting the pagination from the html

                    for (var i = 1; i <= response.count; i++) {
                        if (i % 5 == 0 || i == response.count) {
                            var pageNum = i / 5;
                            if (i % 5 > 0) {
                                pageNum++;
                            }
                            var pagingElement = document.createElement('button'); //Creating the page button
                            pagingElement.setAttribute('id', 'pbtn');
                            pagingElement.onclick = (event) => getPlaylistData(event.target.value * 1) //*
                            pagingElement.innerHTML = Math.floor(pageNum);
                            pagingElement.value = Math.floor(pageNum) // value of the button
                            pagingContainer.appendChild(pagingElement); //adding the button to the container
                        }
                    }

                    $.each(response.models, function (index, value) {
                        if (index == elementPerPage) {
                            return;
                        }
                        row = document.createElement('tr');

                        //Title
                        title = document.createElement('td');
                        title.innerHTML = value.title;

                        //ArtistName
                        artistName = document.createElement('td');
                        artistName.innerHTML = value.artistName;

                        //Duration Minutes
                        durationMinutes = document.createElement('td');
                        const duration = ` ${value.durationMinutes}:${value.durationSeconds} mins`;
                        durationMinutes.innerHTML =(duration);

       
                        // title, rank , duration, genre, play
                        row.appendChild(title);
                        row.appendChild(artistName);
                        row.appendChild(durationMinutes);

                        tableContainer.appendChild(row);
                    });
                }
            }
        );
}





