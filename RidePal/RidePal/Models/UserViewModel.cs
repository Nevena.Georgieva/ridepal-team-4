﻿using RidePal.Services;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Web.Models
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(UserDTO user)
        {
            Id = user.Id;
            Name = user.Name;
            Email = user.Email;
            IsDeleted = user.IsDeleted;
        }

        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        public string Name { get; set; }

        public string Email { get; set; }

        public bool IsDeleted { get; set; }
    }
}
