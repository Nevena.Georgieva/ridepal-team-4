<center><img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/></center>

![](/Assets/Gifs/Intro.gif)
---

# RidePal
> RidePal Playlist Generator is a web application enabling registered users to generate playlists for specific travel duration periods based on their preferred genres. The application offers the option visitors to browse and listen to the playlists created by other users and allows filtering by name, total duration and genre.
---

[**Trello board**](https://trello.com/b/z6SexkBu/ridepal-team-4)
<br/>

---

> The data of the application is stored in a relational database. The application consumes two public REST services in order to achieve its main functionality - Deezer External Service and Microsoft Bing Maps External Service. Initial Seed and subsequent Sync of database is performed only via Admin profile.

<center><img src="Assets/Images/Diagram-models.png" width="800px" /></center>
<center><img src="Assets/Images/Diagram-services.png"" /></center>

---
## Table of Contents

- [Installation](#installation)
- [Technologies](#technologies)
- [Features](#features)
- [Team](#team)

## Installation

### Prerequisites
The following list of software should be installed:
- [SQL Server 2019](https://go.microsoft.com/fwlink/?linkid=866662)
- [.NET Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)

### Clone
- Clone the project using `https://gitlab.com/AtanasV/ridepal-team-4.git`

### Setup

- Navigate to the RidePal folder (e.g. cd /ridepal-team-4/RidePal)
> restore the project and install its dependencies
```Dos
dotnet restore
```
- Database
> updates the database to the last migration
```Dos
dotnet tool install --global dotnet-ef --version *
dotnet ef database update --project .\RidePal.Data\ --startup-project .\RidePal\
```
- Run the project:
> open **new** terminal and run the project
```Dos
dotnet run --project RidePal
```
- Open https://localhost:5001 in your browser
> To seed the Database you need to:
> 1. Login as Admin:
>  - Username: admin@admin.com
>  - Password: admin123
> 2. Click "Sync DataBase" button. Process duration is approximately four minutes.

## Technologies

- ASP.NET Core
- ASP.NET Core Identity
- Entity Framework Core
- MS SQL Server
- JWT for REST API Auth
- Swagger
- Serilog
- MSTest
- MS EF Core In-Memory Database Provider
- GitLab CI
- HTML
- CSS
- Bootstrap
- JavaScript
- AJAX

## Features

#### Home page offers Top 3 of playlists with highest rating, reference to "AboutUs" page and "API documentation" page.

#### Visitors are able to view details and listen to all the available playlists. Browsing playlists experience is improved via filter by name, genre, duration with available instant search. When duration is chosen as filter criteria, the result will be a collection of all playlists with duration +/- 5 minutes from the user input in the search field. When "Generate your own playlist" button is pressed, user is redirected to Login page.

![](/Assets/Gifs/Visitor.gif)
---
<br/><br/>

#### Registered users are able to browse all available playlists just as visitors, they can generate their own playlists by giving title, inserting towns for start and end point of travel, choosing music genres from the available and each genre's share in percentage. Users can edit their playlists's names and also delete them.

![](/Assets/Gifs/User.gif)
---
<br/><br/>

#### Admins are able to edit and delete users, browse playlists, view their details, listen, edit and delete them. Admins can generate their own playlists and also Sinchronize Database with external API.

![](/Assets/Gifs/Admin.gif)
---
<br/><br/>

## Documentation

#### API documentation is accessed only through authentication. Two options are available - via Identity or via JSON Web Token. Admin role is required to access Adm API Controller.

![](/Assets/Gifs/Swagger.gif)
---
<br/><br/>

## Team
* Atanas Velev - [GitLab](https://gitlab.com/AtanasV)
* Nevena Georgieva - [GitLab](https://gitlab.com/Nevena.Georgieva)